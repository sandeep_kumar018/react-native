/* eslint-disable no-unused-vars */

import React, {Component} from 'react';
import {View} from 'react-native';
import SignUp from './components/SignUp';
import Login from './components/Login';
import Home from './components/Home';
import Lobby from './components/Lobby';
import WeeklyWeather from './components/WeeklyWeather';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Login: Login,
    SignUp: SignUp,
    Lobby: Lobby,
    WeeklyWeather: WeeklyWeather,
  },
  {
    initialRouteName: 'Home',
  },
);

const AppContainer = createAppContainer(AppNavigator);

class App extends Component {
  render() {
    return <AppContainer />;
  }
}

export default App;
