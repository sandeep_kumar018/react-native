Weather App
Have the latest weather updates on your fingertips.

Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Prerequisites
Install android studio and react native cli.
Set up an android emulator.
Sign up with open weather app and get an APP id.
Have ngrok installed
Fill in your config/keys.js using the sampleKeys.js template.

Commands to run

react-native start

react-native run-android

Switch to server/src and run
service mongod start
node index.js
