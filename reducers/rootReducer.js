/* eslint-disable no-unused-vars */
import {
  UPDATE_USER,
  UPDATE_COORDS,
  UPDATE_WEATHER,
  UPDATE_PLACE_SEARCHED,
  UPDATE_IS_LOADING,
} from '../actions/types';

const initalState = {
  user: {userName: '', email: '', password: ''},
  placeSearched: '',
  isLoading: '',
  weather: '',
  coords: {latitude: '', longitude: ''},
};

const rootReducer = (state = initalState, action) => {
  switch (action.type) {
    case UPDATE_USER:
      return {
        ...state,
        user: {...state.user, ...action.user},
      };
    case UPDATE_COORDS:
      return {
        ...state,
        coords: action.coords,
      };
    case UPDATE_WEATHER:
      return {
        ...state,
        weather: action.weather,
      };
    case UPDATE_PLACE_SEARCHED:
      return {
        ...state,
        placeSearched: action.placeSearched,
      };
    case UPDATE_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    default:
      return state;
  }
};

export default rootReducer;
