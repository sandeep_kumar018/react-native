/* eslint-disable no-unused-vars */

const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    proxy(['/signup'], {
      //target: 'http://localhost:5000',
      target: ' http://b2da255f.ngrok.io',
    }),
  );
};
