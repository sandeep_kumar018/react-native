/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {View, TextInput} from 'react-native';
import {Button, Header, Text, Input, Icon} from 'react-native-elements';
import keys from '../config/keys';
import axios from 'axios';
import {connect} from 'react-redux';
import {updateUser} from '../actions/index';

class SignUp extends Component {
  async handleOnPress() {
    try {
      const res = await axios.post(keys.ngrok + '/signup', this.props.user);
      if (res.data.success === true) {
        this.props.navigation.navigate('Login');
      }
    } catch (err) {
      console.log(err);
    }
  }
  render() {
    return (
      <View>
        <Text h3 style={{textAlign: 'center'}}>
          Sign Up Form
        </Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>UserName</Text>
          <Input
            style={{height: 40}}
            placeholder="UserName"
            onChangeText={text => {
              this.props.updateUser({userName: text});
            }}
          />
          <Text>Email</Text>
          <Input
            style={{height: 40}}
            placeholder="Email"
            onChangeText={text => {
              this.props.updateUser({email: text});
            }}
          />
          <Text>Password</Text>
          <Input
            secureTextEntry={true}
            style={{height: 40}}
            placeholder="Password"
            onChangeText={text => {
              this.props.updateUser({password: text});
            }}
          />
          <Button title="Submit" onPress={() => this.handleOnPress()} />
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => {
      dispatch(updateUser(user));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUp);
