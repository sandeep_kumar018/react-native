/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Text, Card, Image} from 'react-native-elements';

const styles = StyleSheet.create({
  card: {
    backgroundColor: 'rgba(56, 172, 236, 1)',
    borderWidth: 0,
    borderRadius: 20,
  },
});

class WeatherCard extends Component {
  render() {
    return (
      <View>
        <Card
          titleStyle={{fontSize: 40}}
          title={this.props.title}
          containerStyle={styles.card}
          wrapperStyle={{alignItems: 'center'}}>
          <Image
            style={{width: 100, height: 100}}
            source={{
              uri:
                'https://openweathermap.org/img/w/' + this.props.icon + '.png',
            }}
          />
          <Text h3>{this.props.description}</Text>
          <Text h4>Current Temperature :{this.props.currentTemp}</Text>
          <Text h4>Minimum Temperature :{this.props.minTemp}</Text>
          <Text h4>Maximum Temperature :{this.props.maxTemp}</Text>
        </Card>
      </View>
    );
  }
}

export default WeatherCard;
