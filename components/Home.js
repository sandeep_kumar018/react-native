import React, {Component} from 'react';
import {View} from 'react-native';
import {Text, Button} from 'react-native-elements';

class Home extends Component {
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text h3>Welcome to my weather app</Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Button
            title="Login"
            onPress={() => this.props.navigation.navigate('Login')}
          />
          <Button
            title="SignUp"
            onPress={() => this.props.navigation.navigate('SignUp')}
          />
        </View>
      </View>
    );
  }
}

export default Home;
