/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getWeeklyWeather} from "../api's/weatherApi";
import {View, ImageBackground, FlatList} from 'react-native';
import {Text, SearchBar} from 'react-native-elements';
import WeatherCard from './WeatherCard';
import {
  updateWeather,
  updatePlaceSearched,
  updateIsLoading,
} from '../actions/index';
class WeeklyWeather extends Component {
  handleOnChange = async placeSearched => {
    this.props.updatePlaceSearched(placeSearched);
    const res = await getWeeklyWeather(placeSearched);
    if (res) {
      this.props.updateWeather(res.data);
      this.props.updateIsLoading(false);
    } else {
      this.props.updateIsLoading(true);
    }
  };
  renderHelper() {
    if (this.props.placeSearched === '' && this.props.isLoading === true)
      return (
        <View>
          <Text h3 style={{color: 'white'}}>
            Search for your weather
          </Text>
        </View>
      );
    if (this.props.isLoading === true) {
      return (
        <View>
          <Text h3 style={{color: 'white'}}>
            Sorry! No such place exists.
          </Text>
        </View>
      );
    }
    if (this.props.isLoading === false) {
      this.props.weather.list.map(weatherObj => {
        return (
          <View>
            <WeatherCard
              title={
                this.props.weather.city.name +
                ',' +
                this.props.weather.city.country
              }
              icon={weatherObj.weather[0].icon}
              description={weatherObj.weather[0].main}
              currentTemp={weatherObj.main.temp}
              minTemp={weatherObj.main.temp_min}
              maxTemp={weatherObj.main.temp_max}
            />
            ;
          </View>
        );
      });
    }
  }
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ImageBackground
          source={require('../assets/images/weather.jpg')}
          style={{width: '100%', height: '100%'}}>
          <SearchBar
            placeholder="Search here"
            platform="android"
            onChangeText={placeSearched => this.handleOnChange(placeSearched)}
            value={this.props.placeSearched}
          />
          {this.renderHelper()}
        </ImageBackground>
      </View>
    );
  }
  componentDidMount = () => {};
}

const mapStateToProps = state => {
  return {
    weather: state.weather,
    placeSearched: state.placeSearched,
    isLoading: state.isLoading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateWeather: weather => {
      dispatch(updateWeather(weather));
    },
    updatePlaceSearched: placeSearched => {
      dispatch(updatePlaceSearched(placeSearched));
    },
    updateIsLoading: isLoading => {
      dispatch(updateIsLoading(isLoading));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WeeklyWeather);
