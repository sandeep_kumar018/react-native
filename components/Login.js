/* eslint-disable no-unused-vars */

import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import {Button, Text, Input} from 'react-native-elements';
import keys from '../config/keys';
import axios from 'axios';
import {connect} from 'react-redux';
import {updateUser} from '../actions/index';

class Login extends Component {
  async handleOnPress() {
    try {
      const res = await axios.post(keys.ngrok + '/login', {
        userName: this.props.user.userName,
        password: this.props.user.password,
      });
      if (res.data.success === true) {
        this.props.navigation.navigate('Lobby');
      } else {
        alert('Incorrect UserName or Password');
      }
    } catch (err) {
      alert(err);
      console.log(err);
    }
  }
  render() {
    return (
      <View>
        <Text h3 style={{textAlign: 'center'}}>
          Log In Form
        </Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>UserName</Text>
          <Input
            id="userName"
            ref="userName"
            style={{height: 40}}
            placeholder="UserName"
            onChangeText={text => {
              this.props.updateUser({userName: text});
              AsyncStorage.setItem('userName', text);
            }}
            value={this.props.user.userName}
          />
          <Text>Password</Text>
          <Input
            id="password"
            ref="password"
            secureTextEntry={true}
            style={{height: 40}}
            placeholder="Password"
            onChangeText={text => {
              this.props.updateUser({password: text});
            }}
            value={this.props.user.password}
          />
          <Button title="Submit" onPress={() => this.handleOnPress()} />
        </View>
      </View>
    );
  }
  componentDidMount() {
    console.log(this.props.user);
    if (
      this.props.user.userName.length > 0 ||
      this.props.user.password.length > 0
    ) {
      // React.findDOMNode(this.refs.userName).value = this.props.user.userName;
      // React.findDOMNode(this.refs.password).value = this.props.user.password;
      this.refs.userName = this.props.user.userName;
      this.refs.password = this.props.user.password;
    }
  }
}
const mapStateToProps = state => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => {
      dispatch(updateUser(user));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
