/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {View, ImageBackground} from 'react-native';
import {Text, SearchBar} from 'react-native-elements';
import Geolocation from 'react-native-geolocation-service';
import WeatherCard from './WeatherCard';
import {connect} from 'react-redux';
import {
  updateCoords,
  updateWeather,
  updatePlaceSearched,
  updateIsLoading,
} from '../actions/index';
import {getWeatherByCoordinates, getWeather} from "../api's/weatherApi";

class Lobby extends Component {
  handleOnChange = async placeSearched => {
    this.props.updatePlaceSearched(placeSearched);
    const res = await getWeather(placeSearched);
    if (res) {
      this.props.updateWeather(res.data);
      this.props.updateIsLoading(false);
    } else {
      this.props.updateIsLoading(true);
    }
  };
  getMyLocation = async () => {
    try {
      await Geolocation.getCurrentPosition(
        async position => {
          this.props.updateCoords({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
          const res = await getWeatherByCoordinates(
            this.props.coords.latitude,
            this.props.coords.longitude,
          );
          if (res) {
            this.props.updateWeather(res.data);
            this.props.updateIsLoading(false);
          }
        },
        error => alert(error.message),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );
    } catch (err) {
      console.log(err);
    }
  };
  renderHelper() {
    if (this.props.placeSearched === '' && this.props.isLoading === true)
      return (
        <View>
          <Text h3 style={{color: 'white'}}>
            Search for your weather
          </Text>
        </View>
      );
    if (this.props.isLoading === true) {
      return (
        <View>
          <Text h3 style={{color: 'white'}}>
            Sorry! No such place exists.
          </Text>
        </View>
      );
    }
    if (this.props.isLoading === false) {
      return (
        <View>
          <WeatherCard
            title={
              this.props.weather.name + ',' + this.props.weather.sys.country
            }
            icon={this.props.weather.weather[0].icon}
            description={this.props.weather.weather[0].main}
            currentTemp={this.props.weather.main.temp}
            minTemp={this.props.weather.main.temp_min}
            maxTemp={this.props.weather.main.temp_max}
          />
        </View>
      );
    }
  }
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ImageBackground
          source={require('../assets/images/weather.jpg')}
          style={{width: '100%', height: '100%'}}>
          <SearchBar
            placeholder="Search here"
            platform="android"
            onChangeText={placeSearched => this.handleOnChange(placeSearched)}
            value={this.props.placeSearched}
          />
          {this.renderHelper()}
        </ImageBackground>
      </View>
    );
  }
  async componentDidMount() {
    this.getMyLocation();
  }
}

const mapStateToProps = state => {
  return {
    coords: state.coords,
    weather: state.weather,
    placeSearched: state.placeSearched,
    isLoading: state.isLoading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCoords: coords => {
      dispatch(updateCoords(coords));
    },
    updateWeather: weather => {
      dispatch(updateWeather(weather));
    },
    updatePlaceSearched: placeSearched => {
      dispatch(updatePlaceSearched(placeSearched));
    },
    updateIsLoading: isLoading => {
      dispatch(updateIsLoading(isLoading));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Lobby);
