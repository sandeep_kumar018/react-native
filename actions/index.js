/* eslint-disable no-unused-vars */
import {
  UPDATE_USER,
  UPDATE_COORDS,
  UPDATE_WEATHER,
  UPDATE_PLACE_SEARCHED,
  UPDATE_IS_LOADING,
} from './types';

export const updateUser = user => {
  return {
    type: UPDATE_USER,
    user,
  };
};

export const updateCoords = coords => {
  return {
    type: UPDATE_COORDS,
    coords,
  };
};

export const updateWeather = weather => {
  return {
    type: UPDATE_WEATHER,
    weather,
  };
};

export const updatePlaceSearched = placeSearched => {
  return {
    type: UPDATE_PLACE_SEARCHED,
    placeSearched,
  };
};

export const updateIsLoading = isLoading => {
  return {
    type: UPDATE_IS_LOADING,
    isLoading,
  };
};
