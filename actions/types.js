/* eslint-disable no-unused-vars */
export const UPDATE_USER = 'UPDATE_USER';
export const UPDATE_COORDS = 'UPDATE_COORDS';
export const UPDATE_WEATHER = 'UPDATE_WEATHER';
export const UPDATE_PLACE_SEARCHED = 'UPDATE_PLACE_SEARCHED';
export const UPDATE_IS_LOADING = 'UPDATE_IS_LOADING';
