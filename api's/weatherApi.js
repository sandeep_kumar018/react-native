/* eslint-disable no-empty */
/* eslint-disable no-unused-vars */
import keys from '../config/keys';
import axios from 'axios';

export const getWeatherByCoordinates = async (lat, lon) => {
  const params =
    '?APPID=' + keys.appId + '&units=metric' + '&lat=' + lat + '&lon=' + lon;
  const res = await axios.get(
    'https://api.openweathermap.org/data/2.5/weather' + params,
  );
  return res;
};

export const getWeather = async placeSearched => {
  let params = '?APPID=' + keys.appId + '&units=metric' + '&q=' + placeSearched;
  try {
    const res = await axios.get(
      'https://api.openweathermap.org/data/2.5/weather' + params,
    );
    if (res) {
      return res;
    }
    // eslint-disable-next-line no-empty
  } catch (err) {}
};

export const getWeeklyWeather = async placeSearched => {
  let params = '?APPID=' + keys.appId + '&units=metric' + '&q=' + placeSearched;
  try {
    const res = await axios.get(
      'https://api.openweathermap.org/data/2.5/forecast' + params,
    );
    if (res) {
      return res;
    }
  } catch (err) {}
};
