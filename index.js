/* eslint-disable no-unused-vars */
/**
 * @format
 */

import {AppRegistry, AsyncStorage} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// eslint-disable-next-line no-unused-vars
import {Provider} from 'react-redux';
import configureStore from './store';
import React from 'react';
import {createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {PersistGate} from 'redux-persist/integration/react';
import AsyncStorage1 from '@react-native-community/async-storage';
import rootReducer from './reducers/rootReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

//const store = configureStore(persistedReducer);
const store = createStore(persistedReducer);

let persistor = persistStore(store);

const RNRedux = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent(appName, () => RNRedux);
